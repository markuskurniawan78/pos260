package com.xsis.pos260.repositories;

import com.xsis.pos260.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CategoryRepo extends JpaRepository<Category, Long> {
    @Query("FROM Category WHERE lower(CategoryName) LIKE lower(concat('%',?1,'%') ) ")
    List<Category> SearchCategory(String keyword);
}
