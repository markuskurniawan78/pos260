package com.xsis.pos260.controllers;

import com.xsis.pos260.models.Category;
import com.xsis.pos260.models.Variant;
import com.xsis.pos260.repositories.VariantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiVariantController {

    @Autowired
    private VariantRepo variantRepo;

    @PostMapping(value = "/variant")
    public ResponseEntity<Object> SaveVariant(@RequestBody Variant variant){
        try {
            variant.setCreatedBy("markus");
            variant.setCreatedOn(new Date());
            this.variantRepo.save(variant);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/variant")
    public ResponseEntity<List<Variant>> GetAllVariant() {
        try {
            List<Variant> variantList = this.variantRepo.findAll();
            return new ResponseEntity<>(variantList, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/variant/{id}")
    public ResponseEntity<List<Variant>> GetVariantById(@PathVariable("id") Long id){
        try {
            Optional<Variant> variant = this.variantRepo.findById(id);
            if (variant.isPresent()) {
                // buat entity untuk return
                ResponseEntity rest = new ResponseEntity(variant, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/variant/{id}")
    public ResponseEntity<Object> UpdateVariant(@RequestBody Variant variant, @PathVariable("id") Long id){
        try {
            Optional<Variant> variantData = this.variantRepo.findById(id);

            if (variantData.isPresent()){
                variant.setId(id);
                variant.setModifiedBy("markus");
                variant.setModifiedOn(new Date());
                this.variantRepo.save(variant);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/variant/{id}")
    public ResponseEntity<Object> DeleteVariant(@RequestBody Variant variant, @PathVariable("id") Long Id){
        try {
            Optional<Variant> variantData = this.variantRepo.findById(Id);

            if (variantData.isPresent()){
                variant.setId(Id);
                variant.setModifiedBy("markus");
                variant.setDelete(true);
                variant.setModifiedOn(new Date());
                this.variantRepo.save(variant);
                ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/variantbycategory/{id}")
    public ResponseEntity<List<Variant>> GetAllVariantByCategoryId(@PathVariable("id") Long id)
    {
        try
        {
            List<Variant> variant = this.variantRepo.FindByCategoryId(id);
            return new ResponseEntity<>(variant, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
